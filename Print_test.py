import h5py
import os
from pycbc import waveform
import matplotlib.pyplot as plt

os.system('git lfs clone https://git.ligo.org/derek.white/public-lvcnr-matter.git')
os.system('git lfs fetch --include "/CoRe/CoRe_BAM0001_R01_r00550.h5"')
filepath=os.getcwd() + '/public-lvcnr-matter/CoRe/CoRe_BAM0001_R01_r00550.h5'

fd = h5py.File(filepath, 'r')

params = {}

m1 = fd.attrs['mass1']
m2 = fd.attrs['mass2']

f_low=fd.attrs['f_lower_at_1MSUN']

hp, hc = waveform.get_td_waveform(
    approximant='NR_hdf5',
    numrel_data=filepath, 
    mass1=m1, 
    mass2=m2, 
    distance=1, 
    inclination=0,
    spin1z=fd.attrs['spin1z'], 
    spin1x=fd.attrs['spin1x'], 
    spin1y=fd.attrs['spin1y'], 
    spin2z=fd.attrs['spin2z'], 
    spin2x=fd.attrs['spin2x'], 
    spin2y=fd.attrs['spin2y'], 
    coa_phase=0, 
    long_asc_nodes=0, 
    eccentricity=0, 
    mean_per_ano=0, 
    f_ref=0, 
    f_lower=f_low + 1,
    delta_t=1.0/(16384*8) 
)

fd.close()
plt.plot(hp.sample_times,hp)
plt.show()